package com.emily.EquityTradingPlatformJPA;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class EquityTradingPlatformJpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EquityTradingPlatformJpaApplication.class, args);
	}
}  