package com.emily.EquityTradingPlatformJPA;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Transaction {

	@Id
	private Integer id;
	private Timestamp time = new Timestamp(System.currentTimeMillis());
	private double price;
	private int size;
	private String ticker;
	private String action;

	public Transaction() {

	}

	public Transaction(Integer id, double price, int size, String ticker, String action) {
		super();
		this.id = id;
		this.price = price;
		this.size = size;
		this.ticker = ticker;
		this.action = action;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

}
